<h1>Nuevo juego</h1>
<br><br>
<form class="col-md-12" action="<?php echo site_url('games/guardar_game'); ?>" method="post">
  <div class="row">
    <div class="col-md-4">
      <label for="">Nombres:</label>
      <br>
      <input type="text" name="nombre_game" value="" class="form-control" id="nombre_game" checkedplaceholder="Ingrese el nombre" required>

    </div>
    <div class="col-md-4">
      <label for="">Género:</label>
      <br>
      <input type="text" name="genero_game" value="" class="form-control" id="genero_game" placeholder="Ingrese Género" required>

    </div>
    <div class="col-md-4">
      <label for="">Descripción:</label>
      <br>
      <input type="text" name="descripcion_game" value="" class="form-control" id="descripcion_game" placeholder="Ingrese el Descripción" required>

    </div>
    <div class="col-md-4">
      <label for="">Puntuación:</label>
      <br>
      <input type="number" name="puntuacion_game" value="" class="form-control" id="puntuacion_game" placeholder="Ingrese el Puntuación" required>

    </div>
    <div class="col-md-4">
      <label for="">Modo:</label>
      <br>
      <input type="text" name="modo_game" value="" class="form-control" id="modo_game" placeholder="Ingrese el modo" required>

    </div>

  </div>
  <br><br><br><br>
  <div class="col-md-12 text-center">
    <button type="submit" name="button" class="btn btn-primary">GUARDAR</button>
    &nbsp;
    <a href="<?php echo site_url('games/index'); ?>" class="btn btn-danger">CANCELAR</a>

  </div>

</form>
<br><br><br><br><br>
