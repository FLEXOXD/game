<legend class="text-center">
  <i class="glyphicon glyphicon-user"></i><b> Gestión de Proveedores</b>
  <hr>
  <center>
  <a href="<?php echo site_url('games/nuevo');?>" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Agregar Nuevo</a>
  </center>
  <br>
  <br>
</legend>
<hr>
<?php if ($listadoProveedores): ?>
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">Género</th>
        <th class="text-center">Descripción</th>
        <th class="text-center">Puntuación</th>
        <th class="text-center">Modo</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoProveedores->result() as $proveedorTemporal): ?>
        <tr>
          <td class="text-center"><?php echo $proveedorTemporal->id_game; ?></td>
          <td class="text-center"><?php echo $proveedorTemporal->nombre_game; ?></td>
          <td class="text-center"><?php echo $proveedorTemporal->genero_game; ?></td>
          <td class="text-center"><?php echo $proveedorTemporal->descripcion_game; ?></td>
          <td class="text-center"><?php echo $proveedorTemporal->puntuacion_game; ?></td>
          <td class="text-center"><?php echo $proveedorTemporal->modo_game; ?></td>
          <td class="text-center">
            <a href="<?php echo site_url('games/actualizar'); ?>/<?php echo $proveedorTemporal->id_game; ?>" class="btn btn-warning"><i class="glyphicon glyphicon-edit"></i>Editar</a>
            <a href="<?php echo site_url('games/borrar'); ?>/<?php echo $proveedorTemporal->id_game; ?>" class="btn btn-danger" onclick="return confirm('¿Está seguro de eliminar?');"><i class="glyphicon glyphicon-trash"></i> Eliminar</a></td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <h3 class="text-center"><b>No existen Proveedores</b></h3>
<?php endif; ?>
