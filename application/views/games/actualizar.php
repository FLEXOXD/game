<div class="row">
  <div class="col-md-12 text-center well">
    <h3>ACTUALIZAR GAMES</h3>
    <div class="text-center">
      <a href="<?php echo site_url('games/index') ?>" class="btn btn-primary"><i class="glyphicon glyphicon-arrow-left"></i>Volver</a>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <?php if ($gameEditar): ?>
      <form class="" action="<?php echo site_url('games/actualizacion') ?>" method="post">
      <center><input type="hidden" name="id_game" value="<?php echo $gameEditar->id_game; ?>"></center>
      <div class="row">
        <div class="col-md-4 text-right">
          <label for="">Nombre:</label>

        </div>
        <div class="col-md-7">
          <input type="text" name="nombre_game" value="<?php echo $gameEditar->nombre_game; ?>" class="form-control" placeholder="Ingrese el nombre del juego" required>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-4 text-right">
          <label for="">Género:</label>

        </div>
        <div class="col-md-7">
          <input type="text" name="genero_game" value="<?php echo $gameEditar->genero_game; ?>" class="form-control" placeholder="Ingrese el nombre del juego" required>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-4 text-right">
          <label for="">Descripción</label>

        </div>
        <div class="col-md-7">
          <input type="text" name="descripcion_game" value="<?php echo $gameEditar->descripcion_game; ?>" class="form-control" placeholder="Ingrese el nombre del juego" required>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-4 text-right">
          <label for="">Puntuación</label>

        </div>
        <div class="col-md-7">
          <input type="number" name="puntuacion_game" value="<?php echo $gameEditar->puntuacion_game; ?>" class="form-control" placeholder="Ingrese el nombre del juego" required>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-4 text-right">
          <label for="">Modo:</label>

        </div>
        <div class="col-md-7">
          <input type="text" name="modo_game" value="<?php echo $gameEditar->modo_game; ?>" class="form-control" placeholder="Ingrese el nombre del juego" required>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-4">

        </div>
        <div class="col-md-7">
          <button type="submit" name="button" class="btn btn-warning"><i class="glyphicon glyphicon-ok"></i>Actualizar</button>
          <a href="<?php echo site_url('games/index') ?>" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i>Cancelar</a>

        </div>

      </div>
      </form>
    <?php else: ?>
      <div class="alert alert-danger">
        <b>No se encontro ningun juego</b>

      </div>
    <?php endif; ?>

  </div>

</div>
