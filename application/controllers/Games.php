<?php

/**
 *
 */
class Games extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Game');
  }
  public function index(){
    $data['listadoProveedores'] = $this->Game->obtener_tablas();
    $this->load->view('header');
    $this->load->view('games/index',$data);
    $this->load->view('footer');

  }
  public function nuevo(){
    $this->load->view('header');
    $this->load->view('games/nuevo');
    $this->load->view('footer');

  }
  public function guardar_game()
{
  $data = array(
    'nombre_game' => $this->input->post('nombre_game'),
    'genero_game' => $this->input->post('genero_game'),
    'descripcion_game' => $this->input->post('descripcion_game'),
    'puntuacion_game' => $this->input->post('puntuacion_game'),
    'modo_game' => $this->input->post('modo_game'),
  );

  print_r($data);
  if ($this->Game->guardar_game($data)){
    redirect('games/index');
  }else {
    echo "<h1>ERROR AL GUARDAR</h1>";
  }
}
public function borrar($id_game){
  if ($this->Game->eliminarPorId($id_game)){
    redirect('games/index');
  }else{
      echo "Error al eliminar :(";
  }
}

public function actualizar($id_game){
		$data["gameEditar"]=$this->Game->ObtenerPorId($id_game);
		$this->load->view("header");
		$this->load->view("games/actualizar",$data);
		$this->load->view("footer");
	}
	//Funcion para procesar botón actualización
	public function actualizacion(){
		$datosGameEditado=array(
      "nombre_game"=>$this->input->post('nombre_game'),
			"genero_game"=>$this->input->post('genero_game'),
			"descripcion_game"=>$this->input->post('descripcion_game'),
			"puntuacion_game"=>$this->input->post('puntuacion_game'),
			"modo_game"=>$this->input->post('modo_game')
		);
		$id_game=$this->input->post("id_game");
		if ($this->Game->actualizar($id_game,$datosGameEditado)) {
			redirect('games/index');
		}else{
			echo "<h1>ERROR</h1>";
		}
	}

}
