<?php
/**
 *
 */
class Game extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }
  public function guardar_game($data){
    $this->db->insert('game', $data);
    return $this->db->insert_id();
  }
  public function obtener_tablas(){
    $games=$this->db->get('game');
    if ($games->num_rows()>0) {
        return $games;
      } else {
        return false; //cuando no hay datos

  }
}
public function eliminarPorId($id_game){
  $this->db->where("id_game", $id_game);
          return $this->db->delete("game");
}
public function ObtenerPorId($id_game){
  $this->db->where("id_game",$id_game);
  $games=$this->db->get("game");
  if ($games->num_rows()>0){
    return $games->row();
  } else {
    return false;
  }

}
  public function actualizar($id_game,$data){
    $this->db->where("id_game",$id_game);
    return $this->db->update("game",$data);
  }

}
